# Newomni application

_Docker + Laravel + MySQL + Redis_

## Clone this repo

```bash
git clone git@gitlab.com:stackomni/newomni-backend.git

```

### Run application

```bash
cp .env.example .env
docker-compose up -d
```

#### PHPUnit

Executando os testes

```bash
vendor /bin/phpunit
```
